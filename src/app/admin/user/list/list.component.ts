import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {
  constructor(private http: HttpClient, private router: Router) {

  }

  adminUsers: any = '';
  ngOnInit() {

   
    const user: any = localStorage.getItem('user'); //get browser localstorage data
    const userObj = JSON.parse(user);
    const token = userObj.data.token;
    const headers = new HttpHeaders({
      Authorization: `Bearer ${token}`
    });
    this.http.get('http://localhost:8000/api/user-list', { headers: headers }).subscribe((res: any) => {
      this.adminUsers = res;
      console.log(res);
    }, (err: any) => {
      console.log(err);
    })

  }

}

