import { HttpClient ,HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent {

  
  constructor(private http:HttpClient ,private router:Router) { 

  }
  users:any='';
  
 ngOnInit() {
  const user:any=localStorage.getItem('user'); //get browser localstogare data
  const userObj = JSON.parse(user);
  const token=userObj.data.token;
  const headers = new HttpHeaders({
    Authorization: `Bearer ${token}`
  });
this.http.get('http://localhost:8000/api/user',{headers:headers}).subscribe((res:any) =>{
 this.users=res;
  console.log(res);
},(err:any) =>{
  console.log(err);
})

 }

 logout(){
  localStorage.removeItem('user');
  this.router.navigate(['/']);
 }

}
