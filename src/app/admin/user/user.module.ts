import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { ListComponent } from './list/list.component';
import { AdminNavbarComponent } from './admin-navbar/admin-navbar.component';
import { AdminSidebarComponent } from './admin-sidebar/admin-sidebar.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';


console.log('lazy loading');
@NgModule({
  declarations: [
    ListComponent,
    AdminNavbarComponent,
    AdminSidebarComponent,
    UserEditComponent,
    AdminDashboardComponent,
    
  ],
  imports: [
    CommonModule,
    UserRoutingModule
  ],
 
})
export class UserModule { }
