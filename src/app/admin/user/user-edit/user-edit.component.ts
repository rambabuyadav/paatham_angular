import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent {

 constructor(private http: HttpClient)
 {

 }
 user:any='';
 userObject:any='';
 ngOnInit()
 {
   this.user= localStorage.getItem('user');
   this.userObject = JSON.parse(this.user);
   const token=this.userObject.data.token;
   const headers = new HttpHeaders({
    Authorization: `Bearer ${token}`
   });

 

 }

}
