import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDashboardComponent } from './admin/user/admin-dashboard/admin-dashboard.component';
import { HomeComponent } from './component/frontend/home/home.component';
import { LoginComponent } from './component/frontend/login/login.component';
import { RegisterComponent } from './component/frontend/register/register.component';


const routes: Routes = [
  {path: '', component:HomeComponent},
  {path:'login', component:LoginComponent},
  {path:'register', component:RegisterComponent},
  {path:'dashboard', component:AdminDashboardComponent},
  { path: '',   redirectTo: 'dashboard', pathMatch: 'full' },
  {path:'dashboard/user', loadChildren:()=>import('./admin/user/user.module').then(mod=>mod.UserModule)},
  
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
