import { Component } from '@angular/core';
import { Loginform } from 'src/app/interface/loginform';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  constructor(private http: HttpClient,private router: Router ,private auth:AuthenticationService)
  {

  }

  
  onSubmit(data:Loginform)
  {
    const email = data.email;
    const password = data.password;
     this.auth.login(email,password).subscribe((response:any) => {
      localStorage.setItem('user', JSON.stringify(response));
      this.router.navigate(['/dashboard']);
    
      }, (error: any) => {
        console.log(error);
      })
     
  }

}
