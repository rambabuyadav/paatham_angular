import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/frontend/login/login.component';
import { NavbarComponent } from './component/frontend/navbar/navbar.component';
import { RegisterComponent } from './component/frontend/register/register.component';
import { HomeComponent } from './component/frontend/home/home.component';
import { FooterComponent } from './component/frontend/footer/footer.component';
import { FormsModule } from '@angular/forms';







@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    RegisterComponent,
    HomeComponent,
    FooterComponent,
  
  
  

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
  
  ],

  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
