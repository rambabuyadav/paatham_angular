import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Loginform} from 'src/app/interface/loginform';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient,private router: Router)
  {

  }

  login(email: string, password: string)
  {
    return this.http.post('http://localhost:8000/api/login/', {email, password});
  }
}
